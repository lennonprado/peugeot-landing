import React, { Component } from 'react'
import icon_y from '../assets/icon-youtube-i.png';
import icon_w from '../assets/icon-whatsapp-i.png';
import icon_f from '../assets/icon-financial-i.png';
import icon_d from '../assets/icon-drive-i.png';
import icon_q from '../assets/icon-quiero-i.png';
import icon_fi from '../assets/icon-ficha-i.png';

import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';



export default class Grid extends Component {
  render() {
    return (
      <div className="grid">

        <Row className="fila">
          <Col>
            <a href="ficha.pdf" target="_blank">
              <Card className="mycard">
                <img className="icon" src={icon_fi} alt="Logo" />
                <p>Ficha Tecnica</p>
              </Card>
            </a>
          </Col>
          <Col>
            <a rel="noreferrer" href="https://api.whatsapp.com/send/?phone=542281547070&text=Quiero%20mi%20208" target="_blank">
              <Card className="mycard">
                <img className="icon" src={icon_q} alt="Logo" />
                <p>Quiero mi 208</p>
              </Card>
            </a>
          </Col>
        </Row>

        <Row className="fila">
          <Col>
            <a rel="noreferrer" href="https://www.youtube.com/watch?v=14OAZyszt9o&ab_channel=PeugeotArgentina" target="_blank">
              <Card className="mycard">
                <img className="icon" src={icon_y} alt="Logo" />
                <p>Reseña</p>
              </Card>
            </a>
          </Col>
          <Col>
            <a rel="noreferrer" href="https://www.lemont.com.ar/contacto/testdrive" target="_blank">
              <Card className="mycard">
                <img className="icon" src={icon_d} alt="Logo" />
                <p>Test Drive</p>
              </Card>
            </a>
          </Col>
        </Row>

        <Row className="fila">
          <Col>
            <a rel="noreferrer" href="https://www.psafinance.com.ar/simulador/market/home.do" target="_blank">
              <Card className="mycard">
                <img className="icon" src={icon_f} alt="Logo" />
                <p>Financiación</p>
              </Card>
            </a>
          </Col>
          <Col>
            <a rel="noreferrer" href="https://api.whatsapp.com/send/?phone=542281547070&text=Quiero%20mas%20info%20del%20Peugeot%20208" target="_blank">
              <Card className="mycard">
                <img className="icon" src={icon_w} alt="Logo" />
                <p>Postventa</p>
              </Card>
            </a>
          </Col>
        </Row>

      </div>
    )
  }
}
