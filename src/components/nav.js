import React, { Component } from 'react'

import headerLogo from '../assets/logo_top2m.png';

export default class Nav extends Component {
    render() {
        return (
            <div className="header">

                <img className="logo-header" src={headerLogo} alt="Logo Header" />

                <span className="texto-header"> Peugeot 208 Allure</span>

            </div>
        )
    }
}
