import React from 'react';
import {
  BrowserRouter as Router,
//  Switch,
//  Route
} from "react-router-dom";

//import Container from 'react-bootstrap/Container';
import Nav from './components/nav.js'

import './App.css';

import Grid from './components/grid.js';
import Footer from './components/footer.js';

//import icon-youtube  from "./assets/icon-youtube.png ";
//import icon-youtube  from "./assets/icon-youtube.png ";
//import icon-whatsapp  from "./assets/icon-whatsapp.png ";

//const Home = () => <span>Home</span>;
//const About = () => <span>About</span>;
//const Users = () => <span>Users</span>;

const App = () => (
  <Router>
    <div className="App p-0">
     
      <Nav></Nav>
      <Grid></Grid>
      <Footer></Footer>
{/*   <h2>
        Current Page is{' '}
        <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/users">
            <Users />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </h2>*/}

    </div>
  </Router>
);

export default App;
